# Pre-Requirements
1. https://coral.ai/docs/accelerator/get-started/ NOTE: Installed was used using the max frequency and becomes HOT!!!
2. Python 3.9
3. Edge TPU needs to be plugged in

# Notebooks Utils
- [Google Colab Tensorflow Lite Models](../UTSA_utils/google_colab/tensorflow_lite/)

# Python Scripts
- train_tflite_model.py
    - Will not run if ENV is not used in `conda_env`. Refer to PythonENV readme.md to know how to import
    - This script was used using a laptop with cuda 12.2 and may cause some issues for others.
    - Need to unzip dataset (https://universe.roboflow.com/mftest/footballs-1trlz) as PASCAL VOC

# Models
- soccerball_edgetpu_1.tflite - Currently trained with 1 epoch at 4 batches

# References
- [Python CV Examples TF LITE](https://github.com/NobuoTsukamoto/tflite-cv-example/tree/master/detection)
- [Python CV Examples YOLO](https://github.com/NobuoTsukamoto/tflite-cv-example/tree/master/yolox/python)

# Notes
- YOLOV8 [Class(id=14799, score=0.9972011158242822)]
-   ```bash
    PS C:\Users\Jonahz\Desktop\delete> nvidia-smi
    Sat Nov  4 16:34:42 2023       
    +---------------------------------------------------------------------------------------+
    | NVIDIA-SMI 537.42                 Driver Version: 537.42       CUDA Version: 12.2     |
    |-----------------------------------------+----------------------+----------------------+
    | GPU  Name                     TCC/WDDM  | Bus-Id        Disp.A | Volatile Uncorr. ECC |
    | Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
    |                                         |                      |               MIG M. |
    |=========================================+======================+======================|
    |   0  NVIDIA GeForce RTX 4070 ...  WDDM  | 00000000:01:00.0  On |                  N/A |
    | N/A   45C    P8               2W /  75W |    353MiB /  8188MiB |      4%      Default |
    |                                         |                      |                  N/A |
    +-----------------------------------------+----------------------+----------------------+

    +---------------------------------------------------------------------------------------+
    | Processes:                                                                            |
    |  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
    |        ID   ID                                                             Usage      |
    |=======================================================================================|
    |    0   N/A  N/A     12364    C+G   C:\Windows\explorer.exe                   N/A      |
    +---------------------------------------------------------------------------------------+
    ```