import cv2
import pyrealsense2
from realsense_depth import *


def adj_detect_face(img, df):
    face_cascade = cv2.CascadeClassifier(
        './haarcascades/haarcascade_frontalface_default.xml')
    face_img = img.copy()
    face_rects = face_cascade.detectMultiScale(face_img,
                                               scaleFactor=1.2, minNeighbors=5)

    for (x, y, w, h) in face_rects:
        cv2.rectangle(face_img, (x, y), (x+w, y+h), (255, 255, 255), 10)
        center = (x+w//2, y+h//2)
        radius = 2
        cv2.circle(face_img, center, 4, (0, 0, 255))
        dist = df.get_distance(x+w//2, y+h//2)
        cv2.putText(face_img, "{0:.2f}m".format(dist), (x, y), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2)        

    return face_img


if __name__ == "__main__":
    # Initialize Camera Intel Realsense
    dc = DepthCamera()
    point = (400, 300)
    cv2.namedWindow("Color frame")

    while True:
        ret, depth_frame, color_frame, depth_f = dc.get_frame()

        color_frame = adj_detect_face(color_frame, depth_f)
        cv2.imshow("Color frame", color_frame)
        key = cv2.waitKey(1)
        if key == 27 & 0xFF == ord('q'):
            break