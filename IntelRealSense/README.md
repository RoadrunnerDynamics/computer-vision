# Working Cameras
- D435i (IMU) and D415i

# Getting Started
- Run `find_devices.py` to see if camera is detected

# Setup using Ubuntu 20.04.6 LTS
- `lsb_release -a`
- Follow instructions carefully 
    - [Linux/Ubuntu - RealSense SDK 2.0 Build Guide](https://dev.intelrealsense.com/docs/compiling-librealsense-for-linux-ubuntu-guide?_ga=2.61429292.1897681055.1691874561-212952566.1691874561)
    - Note: I installed everything in my `Documents`
- Verify everything works `realsense-viewer`

# Setup with Windows 10 & 11
- https://github.com/IntelRealSense/librealsense/releases/tag/v2.54.1
- `pip install pyrealsense2`
- Run `find_devices.py` to see if camera is detected

# DOCS
- https://www.intelrealsense.com/depth-camera-d435i/
- https://intelrealsense.github.io/librealsense/python_docs/_generated/pyrealsense2.html

# More Examples
- https://github.com/IntelRealSense/librealsense/tree/master/wrappers/python
- https://github.com/IntelRealSense/librealsense/blob/jupyter/notebooks/distance_to_object.ipynb
- https://github.com/IntelRealSense/librealsense/blob/master/wrappers/python/examples/readme.md

# Debug
- dmesg | grep -i "Camera" (verify that camera can be seen)