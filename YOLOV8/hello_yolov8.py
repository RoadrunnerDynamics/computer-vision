"""hello_yolov8

This script uses the soccerball model built by yolov8n to detect soccerballs
using a computer's built-in camera.
"""

import cv2
import configparser
import torch
from ultralytics import YOLO


if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read('YOLOV8.ini')
    model = YOLO(config["MODEL"]["SOCCERBALL_n"])
    
    cap = cv2.VideoCapture(0)
    while True:
        _, frame = cap.read()
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        if torch.cuda.is_available():
            print("GPU found, using GPU")
            results = model.predict(img, imgsz=384, conf=0.40, device="0") 
        else:
            print("No GPU found, using CPU")
            results = model.predict(img, imgsz=384, conf=0.40)  # Adjust this if too slow

        cv2.imshow('YOLO V8 Detection', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()