"""
Uses built-in Camera with Computer
>>> python .\image_ball_detection.py .\images\soccerball.jpg .20
"""

import argparse
import configparser
import torch
from ultralytics import YOLO


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    parser.add_argument("confidence")
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read('YOLOV8.ini')
    
    model = YOLO(config["MODEL"]["SOCCERBALL_n"])
    if torch.cuda.is_available():
        print("GPU found, using GPU")
        results = model.predict(args.path, imgsz=320, conf=float(args.confidence), device="0") 
    else:
        print("No GPU found, using CPU")
        results = model.predict(args.path, imgsz=320, conf=float(args.confidence))  # Adjust this if too slow
