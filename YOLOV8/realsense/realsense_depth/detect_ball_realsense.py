"""Detects soccerball with YOLOV8 model using intel realsense and and measures distance from camera"""

import cv2
import pyrealsense2
from realsense_depth import *
from ultralytics import YOLO
from ultralytics.yolo.utils.plotting import Annotator


MODEL_PATH = "../../models/soccerball_model.pt"

# Initialize Camera Intel Realsense
dc = DepthCamera()
point = (400, 300)
model = YOLO(MODEL_PATH)

try:
    while True:
        ret, depth_frame, color_frame, depth_f = dc.get_frame()
        results = model.predict(color_frame, imgsz=192, conf=0.25, max_det=1, show=True)
        # color_frame = color_frame.copy()
        for r in results:
            annotator = Annotator(color_frame)
            boxes = r.boxes
            
            for box in boxes:
                xywh = box.xywh.tolist()
                xyxy = box.xyxy.tolist()
                x = int(xywh[0][0])
                y = int(xywh[0][1])
                # center = (int(xywh[0][0]+xywh[0][2])//2, int(xywh[0][1]+xywh[0][3])//2)
                # center = (int(xyxy[0][0]), int( xyxy[0][1]))
                center = ( int((xyxy[0][0] + xyxy[0][2]) / 2), int((xyxy[0][1] + xyxy[0][3]) / 2) )
                cv2.circle(color_frame, center, 10, (0, 0, 255))
                print(center, type(center[0]), type(color_frame))
                b = box.xyxy[0]
                c = box.cls
                annotator.box_label(b, model.names[int(c)])
                # dist = depth_f.get_distance(int(xywh[0][0]+xywh[0][2])//2, int(xywh[0][1]+xywh[0][3])//2)
                # dist = depth_f.get_distance(int(xyxy[0][0]), int( xyxy[0][1]))
                dist = depth_f.get_distance(int((xyxy[0][0] + xyxy[0][2]) / 2), int((xyxy[0][1] + xyxy[0][3]) / 2))
                cv2.putText(color_frame, "{0:.2f}m".format(dist), (x, y), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2)        
        color_frame = annotator.result()
        # cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', color_frame)
        cv2.waitKey(1)
finally:
    dc.release()