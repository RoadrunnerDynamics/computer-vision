# Software
- OpenCV `pip install opencv-python`
- Ultralytics `pip install ultralytics==8.0.150`
- Python >= 3.7 | Started with 3.9.18
- Pyrealsense `pip install pyrealsense2`
- Roboflow `pip install roboflow`
- pip install pyyaml==6.0
- pip install pillow==9.4.0

# Model
- `soccerball_model.pt` has been trained using version 8.0.150 with yolov8n
- `dataset_1.pt` == 8.0.150 with yolov8s

# GPU Training with NVIDIA 4070 RTX on WINDOWS 11
- Follow instructions https://pytorch.org
- CUDA Compute Platform must be the same version https://developer.nvidia.com/cuda-toolkit-archive
- Can use the env using CUDA 11.8 and Stable 2.0.1 (must still download above)

# Training a Model
- Create an account on Roboflow to get access to API key, to download dataset
- [Download pre-trained dataset](https://universe.roboflow.com/mftest/footballs-1trlz)
- [Train Documentation](https://docs.ultralytics.com/usage/python/#train)

# Export Model for Edge TPU Coral
- Run `export_yoloModel.py`
- For windows 11
    - Download WSL https://learn.microsoft.com/en-us/windows/wsl/install
    - Follow instructions for Linunx https://coral.ai/docs/edgetpu/compiler/#download
    - Export to edgetpu
    - Export to tflite first
    - run command `edgetpu_compiler soccerball_model_integer_quant.tflite` on WSL

# Datasets
- [Dataset_1](https://universe.roboflow.com/gaurav-bhat/soccer-ball-rz3st/browse?queryText=&pageSize=50&startingIndex=0&browseQuery=true)

# Debug
- There are times when you need to use a dataset from roboflow and from there you need
to update the version of ultralytics to latest version. This will make all the models work.
Go back to `pip install ultralytics==8.0.150`
- [SSL CERTS ISSUES](https://stackoverflow.com/questions/68275857/urllib-error-urlerror-urlopen-error-ssl-certificate-verify-failed-certifica)
- `yolo settings reset` if issues with training not seeing path

