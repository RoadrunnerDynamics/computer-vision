"""Uses built-in Camera with Computer"""

import cv2
import configparser
import torch
from ultralytics import YOLO
from ultralytics.yolo.utils.plotting import Annotator  # Currently available on ultralytics==8.0.150


if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read('YOLOV8.ini')
    model = YOLO(config["MODEL"]["SOCCERBALL_n"])
    model_imgsz = int(config["DEFAULT"]["imgsz"])
    model_conf = float(config["DEFAULT"]["conf"])
    
    cap = cv2.VideoCapture(0)
    cap.set(3, 640)
    cap.set(4, 480)
    while True:
        _, frame = cap.read()
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        if torch.cuda.is_available():
            print("GPU found, using GPU")
            results = model.predict(img, imgsz=model_imgsz, conf=model_conf, device="0")
        else:
            print("No GPU found, using CPU")
            results = model.predict(img, imgsz=model_imgsz, conf=model_conf)  # Adjust this if too slow

        for r in results:
            annotator = Annotator(frame)
            boxes = r.boxes
            for box in boxes:
                b = box.xyxy[0]
                c = box.cls
                annotator.box_label(b, model.names[int(c)])

        frame = annotator.result()
        cv2.imshow('YOLO V8 Detection', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
