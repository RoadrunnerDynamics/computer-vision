from ultralytics import YOLO
import argparse


# python3 testModelWithImage.py models/dataset_1.pt images/soccerball.jpg 0.20
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("modelPath")
    parser.add_argument("imagePath")
    parser.add_argument("confidence", type=float)
    args = parser.parse_args()

    # Load a pretrained YOLOv8n model
    model = YOLO(args.modelPath)

    # Run inference on 'bus.jpg' with arguments
    model.predict(args.imagePath, imgsz=320, conf=args.confidence, show=True,
                  save=True)
