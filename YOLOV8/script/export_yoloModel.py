from ultralytics import YOLO


# soccer_model.pt was trained using yolov8n. Which is the only model
# I got to compile correctly to utilize TPU usb accelerator
model = YOLO('../models/soccerball_model.pt')
model.export(format='tflite', int8=True)  # Used for EDGE TPU
# model.export(format='edgetpu')  # Used for EDGE TPU only for Linux