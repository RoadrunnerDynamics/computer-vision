import tensorflow as tf
import os
import pathlib
from pycoral.utils import edgetpu
from pycoral.utils import dataset
from pycoral.adapters import common
from pycoral.adapters import classify
from PIL import Image

tf.lite.experimental.Analyzer.analyze(model_path='/home/jonah/Documents/repos/robocup/computer-vision/YOLOV8/models/soccerball_model_saved_model/soccerball_model_full_integer_quant_edgetpu.tflite',
                                      model_content=None,
                                      gpu_compatibility=False)