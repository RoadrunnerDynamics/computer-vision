# Tensorflow Lite
- Everything must be ran on Google Colab. You can run locally, but setup is a pain.
- https://colab.research.google.com/?utm_source=scs-index
    - Try to get the environment with GPU

## TrainTensorflowLiteModel.ipynb
- Trains Model to be to tflite. The reason for is to be utilized with Edge TPU
- Files:
    - [TrainTensorflowLiteModel.ipynb](./TrainTensorflowLiteModel.py)
    - [TrainTensorflowLiteModel.py](./TrainTensorflowLiteModel.py)
    - Datasets:
        - Ball.v2i.voc
        - (https://universe.roboflow.com/mftest/footballs-1trlz)
        - This needs to be to your Google Drive and **Needs to be in PASCAL VOC!!!**

## Compile_TFLite_model_for_EdgeTPU.ipynb
- Used to convert TFLite model to EdgeTPU quantized.
- Files:
    - [Compile_TFLite_model_for_EdgeTPU.ipynb](Compile_TFLite_model_for_EdgeTPU.ipynb)

## YOLO Models
- Currently YOLO is having issues with converting to a tflite model. The outputs are incorrect
and will start getting errors when being utilized with Edge TPU -> (11/4/2023)
- [YOLO Train Custom Data](https://docs.ultralytics.com/yolov5/tutorials/train_custom_data/#train-on-custom-data)

# Docs
- [Tensorflow Lite API](https://www.tensorflow.org/lite/api_docs/python/tflite_model_maker/object_detector/EfficientDetLite0Spec)
- [Edge TPU Compiler](https://coral.ai/docs/edgetpu/compiler/#system-requirements)
- [TF HUB Model Specs](https://tfhub.dev/s?q=efficientdet)
- [Kaggle HUB](https://www.kaggle.com/models/tensorflow/efficientdet/frameworks/tensorFlow2/variations/lite1-feature-vector)
- [MediaPipe Model Training Alternative](https://developers.google.com/mediapipe/solutions/customization/object_detector)
- https://www.tensorflow.org/lite/models/modify/model_maker/object_detection