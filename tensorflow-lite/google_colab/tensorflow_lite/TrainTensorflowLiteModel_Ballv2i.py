# Imports
import numpy as np
import os

from tflite_model_maker.config import ExportFormat, QuantizationConfig
from tflite_model_maker import model_spec
from tflite_model_maker import object_detector

from tflite_support import metadata

import tensorflow as tf
assert tf.__version__.startswith('2')

tf.get_logger().setLevel('ERROR')
from absl import logging
logging.set_verbosity(logging.ERROR)

# Confirm TF Version
print("\nTensorflow Version:")
print(tf.__version__)
print()

# Load Dataset
train_data = object_detector.DataLoader.from_pascal_voc(
    './train',
    './train',
    ['Football']
)

val_data = object_detector.DataLoader.from_pascal_voc(
    './valid',
    './valid',
    ['Football']
)

# Load model spec
# https://tfhub.dev/tensorflow/efficientdet/lite1/feature-vector/1 == efficientdet-lite1
# https://tfhub.dev/tensorflow/efficientdet/lite2/feature-vector/1 == efficientdet-lite2

'''
What is going on here?
- Set train_whole_model=True to fine-tune the whole model instead of just
training the head layer to improve accuracy.
The trade-off is that it may take longer to train the model
https://medium.com/@shroffmegha6695/know-your-neural-network-architecture-more-by-understanding-these-terms-67faf4ea0efb
'''
spec = object_detector.EfficientDetSpec(
  model_name='efficientdet-lite2',
  uri='https://tfhub.dev/tensorflow/efficientdet/lite2/feature-vector/1',
  model_dir='/content/checkpoints',
  hparams={'max_instances_per_image': 8000})  # Max number of detections on one image

# Train the model
# Best outcome has been with batch_size 4, but takes forever
model = object_detector.create(train_data, model_spec=spec, batch_size=32,
                               train_whole_model=True, epochs=50,
                               validation_data=val_data)

# Evaluate the model
eval_result = model.evaluate(val_data)

# Print COCO metrics
print("COCO metrics:")
for label, metric_value in eval_result.items():
    print(f"{label}: {metric_value}")

# Add a line break after all the items have been printed
print()

# Export the model
print("starting export")
model.export(export_dir='.', tflite_filename='TrainTensorflowLiteModel.tflite')
print("done exporting")

# Evaluate the tflite model based on unseen data
tflite_eval_result = model.evaluate_tflite('TrainTensorflowLiteModel.tflite',
                                           val_data)

# Print COCO metrics for tflite
print("COCO metrics tflite")
for label, metric_value in tflite_eval_result.items():
    print(f"{label}: {metric_value}")