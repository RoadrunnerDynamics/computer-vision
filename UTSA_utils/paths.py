import os

def get_parent_directory():
    # get current directory
    path = os.getcwd()
    print("Current Directory", path)
    print()
 
    # parent directory
    parent = os.path.dirname(path)
    print("Parent directory", parent)

    return parent