import cv2

cap = cv2.VideoCapture(4)

while True:
    _, frame = cap.read()
    cv2.imshow('YOLO V8 Detection', frame)
    if cv2.waitKey(1) & 0xFF == ord(' '):
        break

cap.release()
cv2.destroyAllWindows()