import cv2
import configparser

def adj_detect_face(img):
    face_img = img.copy()
    face_rects = face_cascade.detectMultiScale(face_img,
                                               scaleFactor=1.2, minNeighbors=5)

    for (x, y, w, h) in face_rects:
        cv2.rectangle(face_img, (x, y), (x+w, y+h), (255, 255, 255), 10)

    return face_img


if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read("open-cv.ini")
    haarscades_path = config["HAARSCADES"]["FRONTAL_FACE_DEFAULT"]

    face_cascade = cv2.CascadeClassifier(haarscades_path)
    cap = cv2.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        frame = adj_detect_face(frame)
        cv2.imshow('Video Face Detection', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
