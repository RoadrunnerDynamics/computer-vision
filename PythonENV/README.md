# Python Environment
- Contains the environment to code in

# Instructions
1. Download [Anaconda](https://gitlab.com/RoadrunnerDynamics/computer-vision/-/issues/8)
2. Import the .yml using the Import button under "Environments" in Anaconda.

# To Install locally to PC
- pip or pip3 install -r requirements.txt

# To Export a requirements.txt file
- `pip freeze > requirements.txt`

# To Export a Python Environment
- `conda env export --no-builds > "name".yml`
    - Will build env without knowing the python and os
- `conda env export > "name".yml`